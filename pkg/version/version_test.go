package version

import (
	"io/ioutil"
	"log"
	"path/filepath"
	"regexp"
	"strings"
	"testing"
)

func TestVersion(t *testing.T) {
	out, err := ioutil.ReadFile(filepath.Join("..", "..", "VERSION"))
	if err != nil {
		log.Fatal(err)
	}

	repoVersion := strings.TrimRight(string(out), "\r\n")
	progBuildTime := GetBuildTime()
	progVersion := GetVersion()

	if repoVersion != progVersion {
		t.Errorf("Version did not match repo VERSION file, got: %s, want: %s.", progVersion, repoVersion)
	}

	// YYYYmmdd.HHMMSS
	matched, err := regexp.MatchString("\\d{8}\\.\\d{6}", progBuildTime)
	if !matched {
		t.Errorf("Build time did not match the expected syntax, got %s, want: YYYYmmdd.HHMMSS", progBuildTime)
	}
}
